package com.main;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.ArrayList;
import javax.xml.transform.TransformerException;

public class ObscureData {

	
	private static ArrayList<String> patientData,keyboardInput;
	public ObscureData(ArrayList<String> patientData) {
		ObscureData.patientData = patientData;
		ObscureData.keyboardInput = ExtractCSVData.keyboardInput;
	
		//System.out.println("===========================================");
		for (String str : patientData) {
			//System.out.println(str);
		}
	}

	public void obscureData(String path) {
		try {
			extractFolderContent(new File(path));

		} catch (TransformerException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}

	}

	private static void extractFolderContent(File rootFolder) throws TransformerException, ParseException {

		File[] listOfFiles = rootFolder.listFiles();
		try {
			processListOfFiles(listOfFiles);
		} catch (IOException e) {

			e.printStackTrace();
		}
	}

	private static void processListOfFiles(File[] listOfFiles)
			throws IOException, TransformerException, ParseException {
		for (File file : listOfFiles) {
			if (file.isDirectory()) {
				extractFolderContent(file);
			} else if (file.isFile()
					&& ((getFileExtensionName(file).indexOf("txt") != -1)
							|| getFileExtensionName(file).indexOf("csv") != -1)
					|| getFileExtensionName(file).indexOf("owl") != -1) {

				processFile(file);
			}
		}
	}

	public static String getFileExtensionName(File f) {
		if (f.getName().indexOf(".") == -1) {
			return "";
		} else {
			return f.getName().substring(f.getName().length() - 3, f.getName().length());
		}
	}

	private static void processFile(File file) throws IOException, ParseException {

		String content = new String(Files.readAllBytes(Paths.get(file.getAbsolutePath())));
		String newContent = content;
		StringBuffer newContentBuff = null;
		//System.out.println("Replacement in Progress");
		for (String originalString : patientData) {
			newContent = newContent.replaceAll(originalString,"********************************************************");
		}

		for(String inputValue: keyboardInput)
		{
			
			String original = null, replacement = null;
			StringBuilder newValue = new StringBuilder();
			char inputVal[] = inputValue.toCharArray();
			for(int i=0; i<inputVal.length;i++)
			{
				newValue = newValue.append(inputVal[i]);
				original = "Received \\{\"msg_type\":\"keyboard.textUpdate\",\"msg_body\":\\{\"text\":\""+newValue+ "\",\"isOverflow\":false}}";		
				 replacement = "Received \\{\"msg_type\":\"keyboard.textUpdate\",\"msg_body\":\\{\"text\":\""+"============================================"+ "\",\"isOverflow\":false}}";
				 //System.out.println("ORIGINAL: " + original );
				 //System.out.println("replacement: " + replacement );
				 newContent = newContent.replaceAll(original, replacement);
			}
			
		}
		
		newContentBuff = new StringBuffer(newContent);

		if (newContentBuff != null) {
			if (newContentBuff != null)
				newContent = newContentBuff.toString();
			FileOutputStream fooStream = new FileOutputStream(file, false); 
			byte[] myBytes = newContent.getBytes();
			fooStream.write(myBytes);
			fooStream.close();

		}
	}
}
