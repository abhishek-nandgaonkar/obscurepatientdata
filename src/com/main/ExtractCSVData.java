package com.main;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import javax.xml.transform.TransformerException;

public class ExtractCSVData {

	private static ArrayList<String> patientData;
	static ArrayList<String> keyboardInput;
	private static String[] months = { "January", "February", "March", "April", "May", "June", "July", "August",
			"September", "October", "November", "December" };
	private static String[] related = { "Friend", "Sister", "Brother", "Mother", "Father", "Daughter", "Son",
			"Granddaughter", "Grandson", "Aunt", "Uncle", "Cousin", "Other" };

	public ExtractCSVData() {
		super();
		patientData = new ArrayList<String>();
		keyboardInput = new ArrayList<String>();
	}

	public ArrayList<String> extractCSVData(String path) {
		try {
			extractFolderContent(new File(path));
			Set<String> hs = new HashSet<>();
			hs.addAll(patientData);
			patientData.clear();
			patientData.addAll(hs);
			keyboardInput.clear();
			keyboardInput.addAll(hs);
		} catch (TransformerException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return patientData;
	}

	private static void extractFolderContent(File rootFolder) throws TransformerException, ParseException {

		File[] listOfFiles = rootFolder.listFiles();
		try {
			processListOfFiles(listOfFiles);
		} catch (IOException e) {

			e.printStackTrace();
		}
	}

	private static void processListOfFiles(File[] listOfFiles)
			throws IOException, TransformerException, ParseException {
		// handlePropertyTransfersNew = new HandlePropertyTransfersNew();
		for (File file : listOfFiles) {
			if (file.isDirectory()) {
				extractFolderContent(file);
			} else if (file.isFile() && ((getFileExtensionName(file).indexOf("txt") != -1)
					|| getFileExtensionName(file).indexOf("csv") != -1)) {

				processFile(file);
			}
		}
	}

	public static String getFileExtensionName(File f) {
		if (f.getName().indexOf(".") == -1) {
			return "";
		} else {
			return f.getName().substring(f.getName().length() - 3, f.getName().length());
		}
	}

	private static void processFile(File file) throws IOException, ParseException {
		
		BufferedReader br = null;
		String line = "";
		String cvsSplitBy = ",";
		try{
		br = new BufferedReader(new FileReader(file.getAbsolutePath()));
		
		while ((line = br.readLine()) != null) {
			String[] csvLine = line.split(cvsSplitBy);
			if (csvLine.length >= 4) {
				if (csvLine[1].equals("\"ENROLL\"") && csvLine[2].equals("\"KEYBOARD\"")) {
					for (int i = 0; i < csvLine.length; i++) {
						//System.out.print(i + " : " + csvLine[i] + " | ");
						patientData.add(csvLine[3].substring(1, csvLine[3].length()-1));
						keyboardInput.add(csvLine[3].substring(1, csvLine[3].length()-1));
					}
					//System.out.println();
				}
				if (csvLine[1].equals("\"ENROLL\"") && csvLine[2].equals("\"SELECTED\"")
						&& (checkIfExists(months, csvLine[3]) || checkIfExists(related, csvLine[3])
								|| csvLine[3].equals("\"Male\"") || csvLine[3].equals("\"Female\"")
								|| csvLine[3].equals("\"Single\"") || csvLine[3].equals("\"Married\""))) {
					for (int i = 0; i < csvLine.length; i++) {

						//System.out.print(i + " : " + csvLine[i] + " | ");
						patientData.add(csvLine[3].substring(1, csvLine[3].length()-1));

					}
					////System.out.println();
				}
			}
		}}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			br.close();
		}

	}

	public static boolean checkIfExists(String[] array, String str) {
		// String[] words = {"word1", "word2", "word3", "word4", "word5"};
		for (String strings : array) {
			if (strings.equals(str.substring(1, str.length() - 1)))
				return true;

		}
		return false;
	}
}