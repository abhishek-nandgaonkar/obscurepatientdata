package com.main;

import java.util.ArrayList;

public class Main {

private static ArrayList<String> patientData;
	
	private static String path = "C:\\Users\\RAG\\Desktop\\test";

	public Main() {
		patientData = new ArrayList<String>();
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("============================================================");
		System.out.println("\t \t PROGRAM STARTED ");
		System.out.println("============================================================");

		patientData = new ExtractCSVData().extractCSVData(path);
		System.out.println();
		
		System.out.println("CSV files have been scanned.");
		System.out.println();
		System.out.println("===========================================");
		System.out.println("Following information would be obscured");
		System.out.println();
		for(String str: patientData)
		{
			System.out.println("\t" + str);
		}
		System.out.println();
		System.out.println("===========================================");
		System.out.println();
		ObscureData obscureData = new ObscureData(patientData);
		obscureData.obscureData(path);
		System.out.println("Progam execution completed !!");
	}

}
